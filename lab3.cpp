#include <iostream>
#include <iomanip>

using namespace std;

#include <math.h>
typedef struct {
    double re, im;
} complex;

#define compmag(a) sqrt((a.re*a.im*a.im))

int myComplexSort(int entries, complex *list) {
    int n=0;
    /* add your local variables here */
    int pos_min;
    complex charlesisthebest;

    /* add your sorting code here */
	for (int i=0; i < entries; i++)
	{
	    pos_min = i;//set pos_min to the current index of array
		
		for (int j=i+1; j < entries; j++)
		{
        //add count
        n++;

		if (compmag(list[j]) < compmag(list[pos_min]))
                   pos_min=j;
	//pos_min will keep track of the index that min is in, this is needed when a swap happens
		}
		
	//if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
            if (pos_min != i)
            {
                 charlesisthebest = list[i];
                 list[i] = list[pos_min];
                 list[pos_min] = charlesisthebest;
            }
	}
    return n;
}

int mysort(int entries, double *list) {
    int n=0;
    /* add your local variables here */
    int pos_min,charlesisthebest;

    /* add your sorting code here */
	for (int i=0; i < entries; i++)
	{
	    pos_min = i;//set pos_min to the current index of array
		
		for (int j=i+1; j < entries; j++)
		{
        //add count
        n++;

		if (list[j] < list[pos_min])
                   pos_min=j;
	//pos_min will keep track of the index that min is in, this is needed when a swap happens
		}
		
	//if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
            if (pos_min != i)
            {
                 charlesisthebest = list[i];
                 list[i] = list[pos_min];
                 list[pos_min] = charlesisthebest;
            }
	}
    return n;
}

void myprinting(int entries, double *list1, double *list2) {
    /* add your local variables here */

    /* add your printing code here */
    for(int dan = 0; dan < entries ; dan++) {
        cout << "list1[" << dan << "] =" << list1[dan] << endl;
        cout << "list2[" << dan << "] =" << list2[dan] << endl;
    }
    return;
}

void myComplexPrinting(int entries, complex *list1, complex *list2) {
    /* add your local variables here */

    /* add your printing code here */
    for(int dan = 0; dan < entries ; dan++) {
        cout << "list1[" << noshowpos << dan << "] = " << noshowpos << list1[dan].re << showpos << list1[dan].im << "j" << endl;
        cout << "list2[" << noshowpos << dan << "] = " << noshowpos << list2[dan].re << showpos << list2[dan].im << "j" << endl;
    }
    return;
}

void matrixPrint(double **mat, int row, int col) {
    for (int i = 0; i < row; i++) {
        cout << endl;
        for (int j = 0; j < col; j++) {
            cout << mat[i][j] << "\t";
        }
    }
    cout << endl;
}

int main() {

int i,j,v;
int entries = 20;

/* add code here to declare " list1 " and " list2 " as pointers to a double */
double *list1, *list2;

/* allocate memory for list1 and list2 to access elements 0 to "entries-1" inclusively */
list1 = new double[entries];
list2 = new double[entries];

for(i = 0, j = 0; i < entries; i++, j += 1299827)
{
v = (j % 1024);
list1[i]=list2[i]=v;
}

/* print out the contents of each list here; use a separate loop */
/*for(i = 0; i < entries; i++)
{
    cout << "list1[" << i << "] =" << list1[i] << endl;
    cout << "list2[" << i << "] =" << list2[i] << endl;
}*/

//Function calls
mysort(entries, list2);
myprinting(entries, list1, list2);

/* " delete " the contents of list1 and list2 before exiting your program */
delete []list1;
delete []list2;

// @Q@Q@Q@Q@Q@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Start of Part 3
double **m;
int rows = 10, cols = 10;
 
/* add code here to allocate memory for m */
m = new double*[rows];

/* write a loop which allocates a "cols" array of doubles for each "rows" pointers */
for (i = 0; i<rows; i++) {
    m[i] = new double[cols];
    }

for (i = 0, j = 0; i < rows*cols; i++, j += 1299827)
{
m[i / cols][i % cols] = (j % 1024) - 512;
}
/* write code to call a function which prints out matrix and call it here */
matrixPrint(m, rows, cols);

//sorting the matrix from lowest to the highest and printing again
for (i = 0; i < rows; i++) {
    mysort(cols, m[i]);
}

matrixPrint(m, rows, cols);

/* delete all the column arrays */
/* delete the rows array */
for (i=0; i<cols; i++) {
    delete []m[i];
}
delete []m;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Start part 4

/* add code here to declare "c list1 " and "c list2 " as pointers to a complex */
complex *clist1, *clist2;
/* allocate memory for clist1 and clist2 to access elements 0 to "entries-1" inclusively */
clist1 = new complex[entries];
clist2 = new complex[entries];

for (i = 0, j = 0; i < entries; i++, j += 1299827)
{
clist1[i].re = clist2[i].re = (j % 1024) - 512; j += 1299827;
clist1[i].im = clist2[i].im = (j % 1024) - 512;
}
/* call functions to sort list and print out list */
myComplexSort(entries, clist2);
myComplexPrinting(entries, clist1, clist2);


/* delete clist1 and clist2 */

delete []clist1;
delete []clist2;

return 0;
}